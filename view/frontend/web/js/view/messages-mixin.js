define(function($) {
    'use strict';

    return function(target) {
        return target.extend({
            onHiddenChange: function (isHidden) {
                // Leave blank to prevent message from hiding
            }
        });
    };
});
